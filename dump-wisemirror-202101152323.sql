-- MySQL dump 10.18  Distrib 10.3.27-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: wisemirror
-- ------------------------------------------------------
-- Server version	10.3.27-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `department_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` text NOT NULL,
  `department_position` bigint(20) unsigned NOT NULL,
  `department_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (4,'Бухгалтерия',1,0),(5,'Планово-финансовый',2,0),(6,'Дирекция',0,0),(8,'Бизнес-аналитика',3,0),(9,'Отдел по работе с клиентами',4,0),(10,'Маркетинг',5,0),(11,'PR-отдел',6,0),(12,'Дизайн',7,0),(13,'Мобильная разработка',9,0),(14,'Тестирование',10,0),(15,'Системное администрирование',11,0),(16,'Конструкторский отдел',12,0),(17,'Производство',13,0),(18,'Организация документооборота',14,0),(19,'Обучение клиентов и персонала',15,0),(20,'Техническая служба',16,0),(21,'Devops',17,0),(24,'Веб-разработка',8,0);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `employee_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `employee_name` text NOT NULL,
  `employee_position` bigint(20) unsigned NOT NULL,
  `employee_photo_location` varchar(128) NOT NULL DEFAULT 'img/default.png',
  `employee_hidden` tinyint(1) NOT NULL DEFAULT 0,
  `department_id` bigint(20) unsigned NOT NULL,
  `employee_post` text NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `employees_FK` (`department_id`),
  CONSTRAINT `employees_FK` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (23,'Разинкова Надежда',1,'img/team/466740973_Надя.jpg',0,6,'Директор'),(24,'Булгаков Владислав',0,'img/team/1276992385_Влад.jpg',0,6,'Директор'),(25,'Антосяк Максим',0,'img/team/1752328222_МаксимАнтосяк.png',0,4,'Бухгалтер'),(26,'Лесных Виталий',0,'img/default.png',0,4,'Бухгалтер'),(27,'Хуторянский Вадим',0,'img/default.png',0,5,'Финансист'),(28,'Адлейба Эраст',0,'img/team/843129510_ЭрикАдлейба.png',0,8,'Аналитик'),(29,'Душенькин Кирилл',0,'img/team/630243848_КириллДушенькин.png',0,8,'Аналитик'),(30,'Латынина Виктория',0,'img/team/1166899369_ВикторияЛатынина.png',0,9,'Менеджер по работе с клиентами'),(31,'Гордеева Лолита',0,'img/team/403018917_ЛолитаГордеева.png',0,9,'Менеджер по работе с клиентами'),(32,'Киндур Владимир',0,'img/team/56456997_КиндурВладимир.png',0,10,'Маркетолог'),(33,'Калинин Андрей',0,'img/default.png',0,10,'Маркетолог'),(34,'Гармашов Денис',0,'img/team/312995995_ДенисГармашов.png',0,11,'PR-специалист'),(35,'Вощёв Михаил',0,'img/default.png',0,11,'PR-специалист'),(36,'Эймонт Сергей',0,'img/team/89157779_ЭймонтСергей.png',0,12,'Дизайнер'),(37,'Шмыков Никита',0,'img/default.png',0,12,'Дизайнер'),(38,'Разинкова Надежда',1,'img/team/857724148_Надя.png',0,12,'Дизайнер'),(39,'Давыдов Вячеслав',0,'img/default.png',0,24,'Разработчик'),(40,'Телепнев Сергей',0,'img/team/202672054_ТелепневСергей.png',0,24,'Разработчик'),(41,'Насыпалов Никита',0,'img/team/1042127672_НасыпаловНикита.PNG',0,13,'Разработчик'),(42,'Косовцов Николай',0,'img/team/1216850245_КосовцовНиколай.png',0,13,'Разработчик'),(43,'Шуклин Антон',1,'img/team/114329610_ШуклинАнтон.png',0,13,'Разработчик'),(44,'Решетнёв Артём',0,'img/team/109567231_РешетневАртем.PNG',0,14,'Тестировщик'),(45,'Проскурин Андрей',0,'img/team/328204533_ПроскуринАндрей.png',0,14,'Тестировщик'),(46,'Мавлянов Руслан',0,'img/team/818417001_МавляновРуслан.png',0,15,'Системный администратор'),(47,'Вакулин Владислав',0,'img/team/1791150864_ВакулинВлад.png',0,15,'Системный администратор'),(48,'Зенкина Марина',0,'img/team/4684351_ЗенкинаМарина.png',0,16,'Конструктор'),(49,'Михайлюков Андрей',0,'img/team/1148686693_МихайлюковАндрей.png',0,16,'Инженер-конструктор'),(50,'Батищев Павел',1,'img/default.png',0,16,'Конструктор'),(51,'Карелин Владислав',0,'img/team/326273929_КарелинВладислав.png',0,17,'Инженер'),(52,'Тараненко Александр',0,'img/default.png',0,17,'Инженер'),(53,'Раковская Марина',0,'img/team/1379103383_МаринаРаковская.png',0,18,'Менеджер'),(54,'Колесникова Олеся',0,'img/team/854369196_КолесниковаОлеся.png',0,18,'Менеджер'),(55,'Жуков Владислав',0,'img/team/650810627_ВладЖуков.png',0,19,'Менеджер'),(56,'Марков Виталий',0,'img/team/482194302_МарковВиталий.png',0,19,'Менеджер'),(57,'Иванов Кирилл',0,'img/team/1453347932_ИвановКирилл.png',0,20,'Инженер'),(58,'Землянсков Егор',0,'img/default.png',0,20,'Инженер'),(59,'Чушкин Андрей',0,'img/team/653240335_АндрейЧушкин.png',0,21,'DevOps Инженер'),(60,'Махонин Владислав',0,'img/team/671444327_МахонинВячеслав.png',0,21,'DevOps Инженер'),(61,'Горевой Ярослав',1,'img/team/1898761533_ГоревойЯрослав.PNG',0,13,'Разработчик');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_groups`
--

DROP TABLE IF EXISTS `faq_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_groups` (
  `faq_group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `faq_group_title` text NOT NULL,
  `faq_group_position` bigint(20) unsigned NOT NULL,
  `faq_group_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`faq_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_groups`
--

LOCK TABLES `faq_groups` WRITE;
/*!40000 ALTER TABLE `faq_groups` DISABLE KEYS */;
INSERT INTO `faq_groups` VALUES (38,'Группа 1',0,0),(39,'Группа 2',1,0);
/*!40000 ALTER TABLE `faq_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `faq_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `faq_title` text NOT NULL,
  `faq_description` text NOT NULL,
  `faq_group_id` bigint(20) unsigned NOT NULL,
  `faq_position` bigint(20) unsigned NOT NULL,
  `faq_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`faq_id`),
  KEY `faqs_FK` (`faq_group_id`),
  CONSTRAINT `faqs_FK` FOREIGN KEY (`faq_group_id`) REFERENCES `faq_groups` (`faq_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (18,'Вопрос 1','Ответ 1',38,0,0),(19,'Вопрос 2','Ответ 2',38,1,0),(20,'Вопрос 1','Ответ 1',39,0,0),(21,'Вопрос 2','Ответ 2',39,1,0);
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_name` text NOT NULL,
  `order_surname` text NOT NULL,
  `order_email` text NOT NULL,
  `order_address` text NOT NULL,
  `order_comment` text DEFAULT NULL,
  `order_paid` tinyint(1) NOT NULL DEFAULT 0,
  `order_date` datetime NOT NULL,
  `order_sum` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (13,'aaa','bb','email@email','bb','',0,'2021-01-05 17:01:01',5000),(14,'qwf','qwf','qwqw@qf','qwf','qwfqwfqwf',0,'2021-01-05 17:02:12',5000),(15,'qwef','qwf','qwd@qf','qwf','qwf',1,'2021-01-05 17:06:10',0),(16,'aascas','ascasc','wisemirror@mail.ru','ascas','ascasc',1,'2021-01-05 23:07:00',5000),(17,'dscsdv','sdvsd','wisemirror@mail.ru','vsd','vsdvsd',0,'2021-01-05 23:09:30',0),(19,'dsvsv','sdvs','dvo@tuta.io','sdvsdvs','',1,'2021-01-05 23:30:29',10000);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_products`
--

DROP TABLE IF EXISTS `orders_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_products` (
  `order_product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned DEFAULT NULL,
  `product_id` bigint(20) unsigned DEFAULT NULL,
  `product_count` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`order_product_id`),
  KEY `orders_produts_FK` (`product_id`),
  KEY `orders_products_FK` (`order_id`),
  CONSTRAINT `orders_products_FK` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orders_produts_FK` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_products`
--

LOCK TABLES `orders_products` WRITE;
/*!40000 ALTER TABLE `orders_products` DISABLE KEYS */;
INSERT INTO `orders_products` VALUES (4,13,21,1),(5,14,21,1),(6,15,23,3),(7,16,21,1),(8,17,23,1),(10,19,21,2),(11,19,24,4);
/*!40000 ALTER TABLE `orders_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_price` bigint(20) unsigned NOT NULL,
  `product_title` text NOT NULL,
  `product_description` text NOT NULL,
  `product_type` tinyint(1) NOT NULL,
  `product_photo` text NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (18,30000,'Зеркало 1','<h1><span style=\"color:#27ae60\"><strong>Характеристики</strong></span></h1>\r\n\r\n<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" class=\"w-100\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n',0,'img/products/930474467_главноезеркало.jpg'),(19,50000,'Зеркало 2','<p>фвйвйццчсывв ыв фа в я</p>\r\n',0,'img/products/1706164933_зеркало2.jpg'),(20,90000,'Зеркало 3','<p>fcesvesse s gfdbdqwdqwdqw</p>\r\n',0,'img/products/2108831845_зеркало3.jpg'),(21,5000,'Модуль 1','<p>п</p>\r\n',1,'img/products/828115259_зеркало2.jpg'),(22,0,'Модуль 2','<p>у</p>\r\n',1,'img/products/313922041_зеркало3.jpg'),(23,0,'Модуль 3','<p>в</p>\r\n',1,'img/products/1315277369_главноезеркало.jpg'),(24,0,'Модуль 4','<p>а</p>\r\n',1,'img/products/32857726_зеркало2.jpg');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `token_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token_value` text NOT NULL,
  `token_expired` datetime NOT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (7,'d5745f9425eceb269f9fe01d0bef06ff','2021-02-14 18:30:06');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(256) NOT NULL,
  `user_role` varchar(256) NOT NULL,
  `user_password` varchar(256) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','$2y$10$bGJsNW0xUkRqN1BNNUpWMuX72n3s6DYzIutfXXR/ty9edD4CugpGW');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'wisemirror'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-15 23:23:43
