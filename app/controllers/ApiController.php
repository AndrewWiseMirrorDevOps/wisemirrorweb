<?php

use Phalcon\Mvc\Controller;
use Phalcon\Helper\Json;
// use Phalcon\Http\Message\UploadedFile;
// use Phalcon\Session\Manager;
// use Phalcon\Session\Adapter\Stream;
// use Phalcon\Image\Adapter\Gd;

class ApiController extends Controller
{

  public function authAction()
  {

    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    $request =$this->request;
    $login = 'login';
    $password = 'password';
    if ($request->isPost()) {
      $login_mobile =$request->getPost('login');
      $password_mobile = $request->getPost('password');
      if((strcmp($login_mobile,$login) == 0) && (strcmp($password,$password_mobile) == 0)) {
        $token_value = md5($login.$password);
        $token = new Tokens();
        $token->token_value = $token_value;
        $date = date('Y-m-d H:i:s',time()+60*60*24*30);
        $token->token_expired = $date;
        $token->save();
        $array = ['token'=>$token_value];
        echo Json::encode($array);
      } else {
        $array = ['token'=>'error'];
        echo Json::encode($array);
      }
    }
  }

  public function orderAction() {
    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    $request =$this->request;
    if ($request->isPost()) {
      $token = $request->getPost('token');
      $date = date('Y-m-d H:i:s');
      $token_db = Tokens::findfirst("token_value='$token'");
      if(($token_db != NULL) && ($date < $token_db->token_expired)) {

        $newOrder = new Orders();
        $newOrder->order_name = $request->getPost('name');
        $newOrder->order_surname = $request->getPost('surname');
        $newOrder->order_email = $request->getPost('email');
        $newOrder->order_address = $request->getPost('address');
        $newOrder->order_comment = $request->getPost('comment');
        $products_id = $request->getPost('products');
        $products_count = $request->getPost('count');
        $sum = 0;
        $i = 0;
        foreach($products_id as $id) {
          $product = Products::findfirst("product_id='$id'");
          $sum += $products_count[$i] * $product->product_price;
          $i++;
        }

        $newOrder->order_sum = $sum;
        $newOrder->order_date = date('Y-m-d H:i:s');
        $date = $newOrder->order_date;
        if($newOrder->save()) {
          $order = Orders::find("order_date='$date'");
          // $sum=$order[0]->order_sum;
          $id=$order[0]->order_id;
          // $this->session->set('ordered', $sum);
          // $this->session->set('order_id', $id);
          $i=0;
          foreach($products_id as $product) {
            $newOrdersProducts = new OrdersProducts();
            $newOrdersProducts->order_id = $id;
            $newOrdersProducts->product_id = $product;
            $newOrdersProducts->product_count = $products_count[$i];
            $newOrdersProducts->save();
            $i++;
          }
          $array = ['result'=>'success'];
          echo Json::encode($array);
        }
      } else {
          if($token_db != NULL) {
            $token_db->delete();
          }
          $array = ['result'=>'error'];
          echo Json::encode($array);
        }
    }
  }

}

  ?>
